require 'ruby2d'
require 'json'

class Map
  def initialize(screen)
    @screen = screen
    @screen::Image.new('images/mapth2.png', x: 0, y: 0)
  end

  def lat2y(lat)
    return 1962 - 94.6961 * lat
  end
  
  def lng2x(lng)
    return -8584 + 91.28256 * lng
  end
  
  def plot(lat, lng, color = "red")
    @screen::Circle.new(radius: 4.0, x: lng2x(lng), y: lat2y(lat), color: color)
  end
end

# Set the window size
set(width: 920, height: 800, background: "blue")

fights = JSON.parse(File.read("data/fight.json"))
map = Map.new(Window)
index = 0

update do
  if index < fights.length() - 1
    map.plot(fights[index]['lat'].to_f, fights[index]['lng'].to_f)
    index += 1
  end
end

show