/**
 * Run on console browser
 * 
 * 0 Time
 * 1 Lat
 * 2 Lng
 * 3 Course
 * 4 kts
 * 5 mph
 * 6 feet
 * 7 Rate
 */
const dt = document.querySelectorAll('tr.smallrow1:not(.flight_event_facility):not(.flight_event), tr.smallrow2')
let obj = []

for (var d of dt) {
   obj.push({
      time: d.children[0].children[0].innerText,
      lat: d.children[1].children[0].innerText,
      lng: d.children[2].children[0].innerText,
      course: d.children[3].innerText.replace(/\W\s|°/g, ''),
      kts: d.children[4].innerText,
      feet: d.children[6].innerText,
   })
}

console.log(JSON.stringify(obj))