require 'ruby2d'
require 'xmlsimple'

class Map
  def initialize(screen)
    @screen = screen
    @screen::Image.new('images/pyu_map.png', x: 0, y: 0)
  end

  def lat2y(lat)
    return 2622820.35 - 139525.12 * lat
  end
  
  def lng2x(lng)
    return -12843618.4451373000 + 129694.1339199330 * lng
  end
  
  def plot(lat, lng, color = "red")
    @screen::Circle.new(radius: 4.0, x: lng2x(lng), y: lat2y(lat), color: color)
  end
end

# Set the window size
set(width: 923, height: 700, background: "blue")

hash = XmlSimple.xml_in('data/data.gpx')
tracks = hash['trk'][0]['trkseg'][0]['trkpt']

map = Map.new(Window)

tracks.each do |track|
  map.plot(track['lat'].to_f, track['lon'].to_f)
end

show